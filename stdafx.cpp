#include "stdafx.h"

#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"wininet.lib")
#pragma comment(lib,"crypt32.lib")
#pragma comment(lib,"Comctl32.lib")


#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")



#ifdef _DEBUG
#ifdef _WIN64
#define LIBPATHX "f:\\tools\\output\\lib\\vc17\\debug\\x64\\"
#else
#define LIBPATHX "f:\\tools\\output\\lib\\vc17\\debug\\x86\\"
#endif
#else
#ifdef _WIN64
#define LIBPATHX "f:\\tools\\output\\lib\\vc17\\release\\x64\\"
#else
#define LIBPATHX "f:\\tools\\output\\lib\\vc17\\release\\x86\\"
#endif
#endif


// #pragma comment(lib,LIBPATHX "x3.lib")

